from selenium.webdriver.common.by import By

from data.add_remove_element_data import AddRemoveElementData


class AddRemoveElementPage:
    ADD_REMOVE_LINK = (By.LINK_TEXT, AddRemoveElementData.ADD_REMOVE_LINK_TEXT)
    ADD_ELEMENT_BTN = (By.CSS_SELECTOR, "#content > div > button")
    DELETE_BTN = (By.CSS_SELECTOR, "#elements > button")
