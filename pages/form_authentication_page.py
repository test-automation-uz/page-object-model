from selenium.webdriver.common.by import By


class FormAuthenticationPage:
    FORM_AUTH_LOCATOR = (By.LINK_TEXT, "Form Authentication")
    FORM_USERNAME = (By.ID, "username")
    FORM_PASSWORD = (By.ID, "password")
    FORM_SUBMIT_BTN = (By.XPATH, "//button[@type='submit']")
    FORM_ALERT = (By.ID, "flash")
    LOGOUT_BTN = (By.CLASS_NAME, "button")

