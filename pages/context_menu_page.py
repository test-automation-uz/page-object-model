from selenium.webdriver.common.by import By


class ContextMenuPage:
    CONTEXT_MENU = (By.LINK_TEXT, "Context Menu")
    CONTEXT_BOX = (By.ID, "hot-spot")
    PAGE_TITLE = (By.XPATH, "//div[@class='example']/h3")
