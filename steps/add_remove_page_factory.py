from selenium.common import TimeoutException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from seleniumpagefactory import PageFactory


class AddRemoveElementPageFactory(PageFactory):
    def __init__(self, driver):
        super().__init__()
        self.driver = driver
        self._wait = WebDriverWait(self.driver, 10)
        self._action = ActionChains(self.driver)

    locators = {
        'add_remove_link': ('link_text', 'Add/Remove Elements'),
        'add_element_btn': ('css', '#content > div > button'),
        'delete_btn': ('css', '#elements > button')
    }

    def navigate_to_add_remove_page(self):
        self.add_remove_link.click_button()
        return self

    def click_add_element_button(self):
        self.add_element_btn.click_button()
        return self

    def check_delete_button(self):
        try:
            return self.delete_btn.visibility_of_element_located()
        except TimeoutException:
            return 'No Delete Button'

    def click_delete_button(self):
        self.delete_btn.click_button()

        return self
