from selenium.common import TimeoutException

from pages.add_remove_page import AddRemoveElementPage
from steps.common import CommonOps


class AddRemoveElement(CommonOps):
    are = AddRemoveElementPage()

    def navigate_to_add_remove_page(self):
        self.wait_for(self.are.ADD_REMOVE_LINK).click()
        return self

    def click_add_element_button(self):
        self.wait_for(self.are.ADD_ELEMENT_BTN).click()
        return self

    def check_delete_button(self):
        try:
            return self.wait_for(self.are.DELETE_BTN)
        except TimeoutException:
            return "No Delete Button"

    def click_delete_button(self):
        self.find(self.are.DELETE_BTN).click()

        return self
