from steps.add_remove_page_factory import AddRemoveElementPageFactory


class TestPageFactory:
    def test_add_remove_element_factory(self, driver):
        driver.maximize_window()
        driver.get("https://the-internet.herokuapp.com/")

        add_page = AddRemoveElementPageFactory(driver)

        add_page.navigate_to_add_remove_page()
        add_page.click_add_element_button()

        text = add_page.check_delete_button().text

        assert text == "Delete"

        add_page.click_delete_button()

    def test_add_remove_fluent_api(self, driver):
        driver.get("https://the-internet.herokuapp.com")

        add_rem = AddRemoveElementPageFactory(driver)

        (add_rem.navigate_to_add_remove_page()
         .click_add_element_button()
         .click_delete_button())
