from steps.add_remove_step import AddRemoveElement
from steps.context_menu_step import ContextMenu
from steps.dynamic_control_page import DynamicControls
from steps.form_auth_step import FormAuthentication


class TestPageObjectModel:
    def test_form_authentication(self, driver):
        driver.get("https://the-internet.herokuapp.com")

        form = FormAuthentication(driver)

        form.navigate_to_form_page()
        form.enter_login_username("tomsmith")
        form.enter_login_password("SuperSecretPassword!")
        form.click_login_button()

        assert "logged in" in form.check_login_logout_status().text

        form.click_logout_button()
        assert "logged out" in form.check_login_logout_status().text

    def test_add_remove_element(self, driver):
        driver.get("https://the-internet.herokuapp.com")

        add_rem = AddRemoveElement(driver)

        # fluent-api
        (add_rem.navigate_to_add_remove_page()
         .click_add_element_button())

        delete_button = add_rem.check_delete_button().text
        assert delete_button == "Delete"

        add_rem.click_delete_button()
        delete_button = add_rem.check_delete_button()
        assert delete_button == "No Delete Button"

    def test_context_menu(self, driver):
        driver.get("https://the-internet.herokuapp.com")

        context = ContextMenu(driver)
        context.navigate_to_context_menu_page()
        context.right_click_context_menu()

        assert context.check_alert_message() != ""

        context.accept_alert()

        assert context.alert_is_accepted() != ""

    def test_dynamic_control_checkbox(self, driver):
        driver.get("https://the-internet.herokuapp.com")

        checkbox = DynamicControls(driver)
        checkbox.navigate_to_dynamic_controls_page()

        assert checkbox.check_checkbox_element() == "Checkbox Element Presents"
        checkbox.click_control_button("checkbox")

        assert checkbox.check_checkbox_element() == "No Checkbox Element"

        checkbox.click_control_button("checkbox")

        assert checkbox.check_checkbox_element() == "Checkbox Element Presents"

    def test_dynamic_control_input_text(self, driver):
        driver.get("https://the-internet.herokuapp.com")

        input_text = DynamicControls(driver)
        input_text.navigate_to_dynamic_controls_page()

        assert input_text.checking_input_text_element() == False

        input_text.click_control_button("input")

        assert input_text.checking_input_text_element() == True

        input_text.click_control_button("input")

        assert input_text.checking_input_text_element() == False
